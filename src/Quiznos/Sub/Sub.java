package Quiznos.Sub;

import Quiznos.Meat.Meat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sub
{
    private static final int BASE_PRICE=200;
    private List<Meat> meats;
    private int totalPrice;

    public Sub(){
        meats=new ArrayList<>();
        totalPrice=BASE_PRICE;
    }

    public int getTotalPrice(){
        return totalPrice;
    }
    public void addMeat(Meat meat){
        meats.add(meat);
        totalPrice+=meat.getPrice();
    }
    public String toString(){
        return "Total Price: " +this.totalPrice +"c\n" +
                "Meats: " +meats.stream().map(Meat::toString).collect(Collectors.joining(","));
    }
}
