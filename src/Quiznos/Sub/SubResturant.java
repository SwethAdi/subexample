package Quiznos.Sub;

import Quiznos.Exception.InvalidMeatException;
import Quiznos.Meat.Chicken;
import Quiznos.Meat.Ham;
import Quiznos.Meat.Meat;
import Quiznos.Meat.Turkey;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SubResturant {
    private static final String MEAT_CHOICES = "Please select a meat choice: \n"+
            "1: Ham for " + Ham.PRICE + "c\n" +
            "2: Chicken for " + Chicken.PRICE + "c\n" +
            "3: Turkey for " + Turkey.PRICE + "c\n";

    private String name;

    public SubResturant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Sub buildSub(){
        Sub sub = new Sub();
        char addAnother = 'y';
        Scanner scanner = new Scanner(System.in);
        int choice = 0;
        Meat meat;

        while(addAnother == 'y'){
            boolean validChoice = false;

            while (!validChoice){
                try {
                    System.out.println(SubResturant.MEAT_CHOICES);
                    choice = scanner.nextInt();
                    validChoice = true;
                } catch (InputMismatchException ignored){
                    System.out.println("Invalid choice, please try again");
                } finally {
                    scanner.nextLine();
                }
            }
            scanner.nextLine();
            switch (choice){
                case 1:
                    meat = new Ham();
                    break;
                case 2:
                    meat = new Chicken();
                    break;
                case 3:
                    meat = new Turkey();
                    break;
                default:
                    throw new InvalidMeatException("Invalid meat option selected");
            }
            sub.addMeat(meat);

            System.out.println("Would you like to add another meat? (y/n)");
            addAnother = scanner.nextLine().charAt(0);
        }
        return sub;
    }

    public void run(){
        Sub sub = buildSub();
        System.out.println("Thank you for visiting " + this.name + " here is your sub: \n" + sub);
    }
}
