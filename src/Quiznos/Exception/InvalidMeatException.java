package Quiznos.Exception;

public class InvalidMeatException extends RuntimeException {
    public InvalidMeatException(String message){
        super(message);
    }
}