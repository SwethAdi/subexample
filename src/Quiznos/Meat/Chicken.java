package Quiznos.Meat;

public class Chicken extends Meat {
    public final static int PRICE = 600;

    public Chicken() {
        super(Chicken.PRICE);
    }
}
