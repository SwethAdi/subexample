package Quiznos.Meat;

public abstract class Meat {
    protected int price;

    public Meat(int price){
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String toString(){
        return getClass().getSimpleName() + ": " + getPrice() + "c";
    }
}
