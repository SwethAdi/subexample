package Quiznos.Meat;

public class Ham extends Meat {
    public final static int PRICE = 400;

    public Ham() {
        super(Ham.PRICE);
    }
}