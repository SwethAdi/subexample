package Quiznos.Meat;

public class Turkey extends Meat {
    public final static int PRICE = 700;

    public Turkey() {
        super(Turkey.PRICE);
    }
}
